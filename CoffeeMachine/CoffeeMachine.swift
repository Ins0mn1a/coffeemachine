//
//  CoffeeMachine.swift
//  CoffeeMachine
//
//  Created by Grisha Stetsenko on 29.08.2018.
//  Copyright © 2018 Grisha Stetsenko. All rights reserved.
//

import UIKit

class CoffeeMachine: NSObject {
    
    var water = 0
    var milk = 0
    var coffeeBeans = 0
    var beansTray = 0
    let beansTrayMax = 200
    
    let needWaterForCoffee = 200
    let needCoffeeBeansForCoffee = 18
    let beansTrayAfterCoffee = 45
    
    let needWaterForAmericano = 100
    let needMilkForAmericano = 100
    let needCoffeeBeansForAmericano = 16
    let beansTrayAfterAmericano = 42
    
    override var description: String {
        let result = "SuperCoffeeMaker3000"
        return result
    }
    
    func fillTheWatter() {
        water += 500 //ml
    }
    
    func fillTheMilk() {
        milk += 200 //ml
    }
    
    func fillTheBeans() {
        coffeeBeans += 150 //gramm
    }
    
    func clearCoffeeTray() {
        beansTray = 0
    }
    
    func makeCoffee() {
        if water >= needWaterForCoffee {
            if coffeeBeans >= needCoffeeBeansForCoffee {
                if (beansTray + beansTrayAfterCoffee) <= beansTrayMax {
                    water -= needWaterForCoffee
                    coffeeBeans -= needCoffeeBeansForCoffee
                    beansTray += beansTrayAfterCoffee
                    print("There is your coffee, sir!")
                }
                else {
                    print("Clean the coffee beans tray!")
                }
            }
            else {
                print("Not enough coffee beans! Need \(needCoffeeBeansForCoffee - coffeeBeans) grams more of coffee beans to make a coffee.")
            }
        }
        else {
            print("Not enough water! Need \(needWaterForCoffee - water) ml more of water to make a coffee.")
        }
    }
    
    func makeAmericano() {
        if water >= needWaterForAmericano {
            if coffeeBeans >= needCoffeeBeansForAmericano {
                if milk >= needMilkForAmericano {
                    if (beansTray + beansTrayAfterAmericano) <= beansTrayMax {
                        water -= needWaterForAmericano
                        coffeeBeans -= needCoffeeBeansForAmericano
                        milk -= needMilkForAmericano
                        beansTray += beansTrayAfterAmericano
                        print("There is your americano, sir!")
                    }
                    else {
                        print("Clean the coffee beans tray!")
                    }
                }
                else {
                    print("Not enough milk! Need \(needMilkForAmericano - milk) ml of milk more to make americano")
                }
            }
            else {
                print("Not enough coffee beans! Need \(needCoffeeBeansForAmericano - coffeeBeans) grams of coffee beans more to make a americano.")
            }
        }
        else {
            print("Not enough water! Need \(needWaterForAmericano - water) ml of water more to make a americano.")
        }
    }
}
