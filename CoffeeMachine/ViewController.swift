//
//  ViewController.swift
//  CoffeeMachine
//
//  Created by Grisha Stetsenko on 29.08.2018.
//  Copyright © 2018 Grisha Stetsenko. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        checkAllImages()
    }
    
    @IBOutlet weak var CMmilkImage: UIImageView!
    @IBOutlet weak var CMbeansImage: UIImageView!
    @IBOutlet weak var CMwaterImage: UIImageView!
    @IBOutlet weak var CMempty: UIImageView!
    @IBOutlet weak var topLabel: UILabel!
    var text = ""
    var water = 0
    var milk = 0
    var coffeeBeans = 0
    var beansTray = 0
    let beansTrayMax = 200
    
    let needWaterForCoffee = 200
    let needCoffeeBeansForCoffee = 20
    let beansTrayAfterCoffee = 50
    
    let needWaterForAmericano = 200
    let needMilkForAmericano = 100
    let needCoffeeBeansForAmericano = 20
    let beansTrayAfterAmericano = 50
    
    override var description: String {
        let result = "SuperCoffeeMaker3000"
        return result
    }
    
    func checkAllImages() {
        checkMilkImage()
        checkBeansImage()
        checkWaterImage()
    }
    
    func checkWaterImage() {
        if water > 0 {
            let imageWater = UIImage.init(named: "CM-water1")
            CMwaterImage.image = imageWater
        }
        else {
            let imageWater = UIImage.init(named: "")
            CMwaterImage.image = imageWater
        }
    }
    
    func checkMilkImage() {
        if milk > 0 {
            let imageMilk = UIImage.init(named: "CM-milk1")
            CMmilkImage.image = imageMilk
        }
        else {
            let imageMilk = UIImage.init(named: "")
            CMmilkImage.image = imageMilk
        }
    }
    
    func checkBeansImage() {
        if coffeeBeans > 0 {
            let imageBeans = UIImage.init(named: "CM-beans1")
            CMbeansImage.image = imageBeans
        }
        else {
            let imageBeans = UIImage.init(named: "")
            CMbeansImage.image = imageBeans
        }
    }
    
    @IBAction func fillTheWatter() {
        water += 400 //ml
        topLabelText("You filled 400 ml of water")
        checkWaterImage()
    }
    
    @IBAction func fillTheMilk() {
        milk += 200 //ml
        topLabelText("You filled 200 ml of milk")
        checkMilkImage()
    }
    
    @IBAction func fillTheBeans() {
        coffeeBeans += 100 //gramm
        topLabelText("You filled 100 grams of coffee beans")
        checkBeansImage()
    }
    
    @IBAction func clearCoffeeTray() {
        beansTray = 0
        topLabelText("Coffee tray is empty now")
    }
    
    func topLabelText(_ message: String) {
        text = message
        topLabel.text = text
    }
    
    @IBAction func makeAmericano() {
        if water >= needWaterForAmericano {
            if coffeeBeans >= needCoffeeBeansForAmericano {
                if milk >= needMilkForAmericano {
                    if (beansTray + beansTrayAfterAmericano) <= beansTrayMax {
                        water -= needWaterForAmericano
                        coffeeBeans -= needCoffeeBeansForAmericano
                        milk -= needMilkForAmericano
                        beansTray += beansTrayAfterAmericano
                        topLabelText("There is your americano, sir!")
                    }
                    else {
                        topLabelText("Clean the coffee beans tray!")
                    }
                }
                else {
                    topLabelText("Not enough milk! Need \(needMilkForAmericano - milk) ml of milk more to make americano")
                }
            }
            else {
                topLabelText("Not enough coffee beans! Need \(needCoffeeBeansForAmericano - coffeeBeans) grams of coffee beans more to make a americano.")
            }
        }
        else {
            topLabelText("Not enough water! Need \(needWaterForAmericano - water) ml of water more to make a americano.")
        }
        checkAllImages()
    }

    @IBAction func makeCoffee() {
        if water >= needWaterForCoffee {
            if coffeeBeans >= needCoffeeBeansForCoffee {
                if (beansTray + beansTrayAfterCoffee) <= beansTrayMax {
                    water -= needWaterForCoffee
                    coffeeBeans -= needCoffeeBeansForCoffee
                    beansTray += beansTrayAfterCoffee
                    topLabelText("There is your coffee, sir!")
                }
                else {
                    topLabelText("Clean the coffee beans tray!")
                }
            }
            else {
                topLabelText("Not enough coffee beans! Need \(needCoffeeBeansForCoffee - coffeeBeans) grams of coffee beans more to make a coffee.")
            }
        }
        else {
            topLabelText("Not enough water! Need \(needWaterForCoffee - water) ml of water more to make a coffee.")
        }
       checkAllImages()
    }
}

